from django.shortcuts import render
from.models import Jugadores
from django.db import IntegrityError

# Create your views here.

def cargar_jugadores(request):
    lista = Jugadores.objects.all()

    if request.method =='POST':

        try:
            nombre = request.POST.get('nombre')
            numero = request.POST.get('numero')
            cargar_jugador_base(nombre,numero)

        except IntegrityError as otroErr:
            err = "El jugador no puede repetir"
            return render(request, 'cargar_jugadores.html',{'lista': lista, 'error': str(err)})

        except Exception as err:
            return render(request, 'cargar_jugadores.html',{'lista': lista, 'error': str(err)})


    return render(request, 'cargar_jugadores.html',{'lista': lista})




