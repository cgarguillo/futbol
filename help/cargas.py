from futbol.models import Jugadores

def cargar_jugador_base(nombre, numero):     # obtengo los datos del request
    if int(numero) > 11:    # validacion si es mayor que 11 tira error
        raise Exception("Error futbolista no puede tener un numero a 11")
    jugadores = Futbolista()
    jugadores.nombre = nombre
    jugadores.numero = numero
    jugadores.save()
